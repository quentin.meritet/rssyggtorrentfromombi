
import sqlite3
from sqlite3 import Error
import subprocess
import feedparser
import cloudscraper
import os
import requests
import http.client
import mimetypes
import json
import telebot
from telethon.sync import TelegramClient
from telethon.tl.types import InputPeerUser, InputPeerChannel
from telethon import TelegramClient, sync, events
import smtplib, ssl

# Ce programme permet de recuperer les films que l'on a ajoute sur ombi, de verifier s'ils sont disponible sur yggtorrent a partir du flux rss

# TMDB
apiKeyTMDB = '' # API Key from TMDB
queryMovieTMDB = "http://api.themoviedb.org/3/search/movie?api_key="+apiKeyTMDB+"&query="

# yggtorrent
passkeyYggtorrent = '' 

# IFTTT
keyIFTTT = '' # IFTTT private key
urlIFTTTstart = "curl -X POST -H \"Content-Type: application/json\" -d '{\"value1\":\""
urlIFTTTstop = "\"}' https://maker.ifttt.com/trigger/yggtorrentTelegram/with/key/"


# Docker Ombi
idContainerOmbi = "" # Docker container ID of Ombi
pathToDB = "/home/pi/scripts/Ombi/db" # Path to copy Ombi database
dbFileOmbi = pathToDB+"/Ombi.db"
scriptCopyDBOmbi = "docker cp "+idContainerOmbi+":/config/Ombi.db "+pathToDB
urlOmbi = "http://localhost:8191/v1"

# FileBot
pathToMoviesFiles = "/home/pi/scripts/Ombi/films/" # Temporary path to movie text file 
scriptToRenameFiles = "filebot -rename "+pathToMoviesFiles+" --db TheMovieDB -no-index  -non-strict" # Use Filebot to rename the movies

# General
idListOmbi = []
titleListYgg = []
idListYgg = []
titleMoviesFound = []
pathToXmlYGGRSS = "/home/pi/scripts/Ombi/parsexml.xml" # temporary File --> RSS file from yggtorrent

# Telegram
api_id = 0000000
api_hash = ''
token = ':'
phone = ''
telegramEntity = '' # name of personn or bot


############################## OMBI ###########################################

def GetOmbiDB():
    process = subprocess.Popen(scriptCopyDBOmbi.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def selectAllIDFromDB(conn):
    cur = conn.cursor()
    cur.execute("SELECT TheMovieDbId FROM MovieRequests WHERE Available=0")

    rows = cur.fetchall()

    for row in rows: # TODO : try catch 
        row = str(row).replace('(','')
        row = str(row).replace(')','')
        row = str(row).replace(',','')
        idListOmbi.append(row)
        print(row)

#############################YGGTORRENT - RSS #################################
def parseRSS(xmlToParse):
    titleList = []
    feed = feedparser.parse(xmlToParse)
    for entry in feed.entries:
        title = entry.title
        size = len(title)
        titleList.append(title[:size - 10])
    return titleList
   
def GetRSS():
    print("begin")
    xmlYGGRSS = []
    url = urlOmbi

    payload = "{\r\n    \"cmd\": \"request.get\",\r\n    \"url\": \"https://www4.yggtorrent.li/rss?action=generate&type=subcat&id=2183&passkey="+ passkeyYggtorrent+ "\"\r\n}"
    headers = {
      'Content-Type': 'text/plain'
    }

    response = requests.request("POST", url, headers=headers, data = payload)
    j =  json.loads(response.text)
    # TODO : try catch 
    xmlYGGRSS = j['solution']['response']
    text_file = open(pathToXmlYGGRSS, "w+")
    text_file.write(xmlYGGRSS)
    text_file.close()



####################### File Bot #############################################
def renameWithFileBot():
    process = subprocess.Popen(scriptToRenameFiles.split(),stdout=subprocess.PIPE)
    output, error = process.communicate()
    print("rename...")

def fileToList(titleList):
    scriptToExec = ""
    index = 0
    for title in titleList:
        scriptToExec = "touch '"+pathToMoviesFiles+title.replace('""','').replace('/',' ')+".mkv'"
        print(scriptToExec)
        os.system(scriptToExec)


    renameWithFileBot()
    renametitleList = os.listdir(pathToMoviesFiles)
    newRenameTitleList = []
    for title in renametitleList:
        if (title.find('(') > -1):
            index = title.index('(')
            title = title[:index]
        newRenameTitleList.append(title)
    return newRenameTitleList

##################### TMDB ###################################################
def getTMDBIDfromTitle(title):
    r = requests.get(url = queryMovieTMDB+title.replace(' ','+'))
    data = r.json()
    idlist = []
    for result in data['results']:
        idlist.append(result['id'])
    return idlist

########################### TELEGRAM #########################################
def sendMessageTelegram(mymessage):
    client = TelegramClient('session_name', api_id, api_hash)
    client.start()
    print(client.get_me().stringify())

    client.send_message(entity=telegramEntity, message=mymessage)
    client.disconnect()

########################## IFTTT ############################################
def sendIFTTT(title):
    myurl = urlIFTTTstart + title + urlIFTTTstop
    os.system(myurl) 

############################ MAIN #############################################
def main():

    GetOmbiDB()
    conn = create_connection(dbFileOmbi)
    selectAllIDFromDB(conn)
    GetRSS()
    titleListYgg =parseRSS(pathToXmlYGGRSS)
    titleListYgg = fileToList(titleListYgg)
    for title in titleListYgg:
        idlist = getTMDBIDfromTitle(title)
        for idMovie in idlist:
            if idMovie in idListOmbi:
                print(title)
                sendMessageTelegram("Nouveau film de disponible : "+title)
                sendIFTTT(title)
    scriptToExec = "rm /home/pi/scripts/Ombi/films/*"
    os.system(scriptToExec)
    scriptToExec = "rm /home/pi/scripts/Ombi/parsexml.xml"
    os.system(scriptToExec)
    sendMessageTelegram("Treatment Done")


if __name__ == "__main__":
    main()
